using authentication_api.Entities;
using authentication_api.Models;

namespace authentication_api.Services
{
    public interface IUsersService
    {
        User GetById(int id);
        AuthenticateResponse Authenticate(AuthenticateRequest model);
    }
}