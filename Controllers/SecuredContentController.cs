using System.Collections.Generic;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;

namespace authentication_api.Controllers
{
    
    [ApiController]
    [Route("[controller]")]
    public class SecuredContentController: ControllerBase
    {
        [Authorize]
        [HttpGet]
        public IActionResult Get()
        {
            var dictionary = new Dictionary<string, string>
            {
                ["message"] = "Secured content"
            };
            string json = JsonSerializer.Serialize(dictionary);
            return Ok(json);
        }
    }
}