using authentication_api.Entities;
using authentication_api.Models;
using authentication_api.Services;
using Microsoft.AspNetCore.Mvc;

namespace authentication_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController: ControllerBase
    {
        private IUsersService usersService;

        public UserController(IUsersService userService)
        {
            this.usersService = userService;
        }


        [HttpPost("signup")]
        public IActionResult SignUp(AuthenticateRequest model)
        {
            //Hardcoded for this demo purpose. Todo: remove this condition
            if(model == null || model.Username != "user" || model.Password != "password"){
                return BadRequest(new { message = "Error while signing up" });
            }
            User user = new User{
                Id = 1,
                Username = "user",
            };
            return CreatedAtAction(null, new {id = user.Id}, user);
        }

        [HttpPost("authenticate")]
        public IActionResult Authenticate(AuthenticateRequest model)
        {
            var response = usersService.Authenticate(model);
            if (response == null)
                return BadRequest(new { message = "Incorrect Username / password" });
            return Ok(response);
        }
    }
}