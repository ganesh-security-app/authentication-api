namespace authentication_api.Helpers
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public int tokenExpiryInMilliseconds {get; set;}
    }
}