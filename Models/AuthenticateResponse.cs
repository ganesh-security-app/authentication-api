
using authentication_api.Entities;

namespace authentication_api.Models
{
    public class AuthenticateResponse
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
    
        public AuthenticateResponse(User user, string token)
        {
            Id = user.Id;
             Username = user.Username;
            Token = token;
        }

    }


}